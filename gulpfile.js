var gulp = require('gulp');
var shell = require('gulp-shell')

gulp.task('default', shell.task([
    './ZXPSignCmd -sign extension kiki.zxp kiki.p12 kiki',
    'unzip kiki.zxp -d kiki',
    'rm -fr "/Library/Application Support/Adobe/CEP/extensions/fr.sebcreme.cclive"',
    'cp -r kiki "/Library/Application Support/Adobe/CEP/extensions/fr.sebcreme.cclive"',
    'rm -fr kiki',
    'rm kiki.zxp'
]));


gulp.task('watch', function () {
   gulp.watch('extension/*', ['default']);
});